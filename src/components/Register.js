import React, { useState } from "react";
import {Link } from 'react-router-dom'
import Login from './Login';
// import {useNavigate} from 'react-router-dom';

function Register() {
  // const navigate = useNavigate();
    const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");
  const [nameErr, setNameErr] = useState(false);
  const [emailErr, setEmailErr] = useState(false);

  const [passwordErr, setPasswordErr] = useState(false);

  const onSubmit = (e) => {
    setNameErr(false);
    setEmailErr(false);
    if (name === "") {
      setNameErr(true);
    }
    if (email === "") {
      setEmailErr(true);
    }

    if (password === "") {
      setPasswordErr(true);
    }

    e.preventDefault();
    if (name && email && password) {
      debugger
      console.log(name, email, password);
      debugger;
      let obj = { username: name, emailId: email, pwd: password };
      localStorage.setItem("data", JSON.stringify(obj));
      // navigate('/login')
      window.location="/login" 
     }
  };
  return (
    <div>
      <form autoComplete="off" onSubmit={onSubmit}>
        <div class="mb-3">
          <label for="username" class="form-label">
            Name
          </label>
          <input
            type="text"
            class="form-control"
            id="username"
            required
            error={nameErr}
            aria-describedby="emailHelp"
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div class="mb-3">
          <label for="emailId" class="form-label">
            Email
          </label>
          <input
            type="email"
            class="form-control"
            id="emailId"
            required
            error={emailErr}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">
            Password
          </label>
          <input
            type="password"
            class="form-control"
            id="password"
            required
            error={passwordErr}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button type="submit" class="btn btn-primary">
          Register
        </button>
      </form>
    </div>
  );
}

export default Register;
