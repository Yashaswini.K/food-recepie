import React, { useState } from "react";
import {Link} from 'react-router-dom';


function Login() {
  // const navigate = useNavigate();

    const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");
  const onSave = (e) => {
   console.log("data")
  //  <Link to="/contact">Contact</Link> 
//   history.push ('./pages/MyComponent');
   
  }
  return (
  <div>
  <form autoComplete="off" onSubmit={onSave}>
      
        <div class="mb-3">
          <label for="email" class="form-label">
            Email
          </label>
          <input
            type="email"
            class="form-control"
            id="email"
            required
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div class="mb-3">
          <label for="pwd" class="form-label">
            Password
          </label>
          <input
            type="password"
            class="form-control"
            id="pwd"
            required
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button type="submit" class="btn btn-primary">
          Login
        </button>
      </form>
  </div>
  );
}

export default Login;
