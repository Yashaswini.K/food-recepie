import React, { useState, useEffect } from "react";
import axios from "axios";

export default function BlogComponent() {
  const [inputVal, setInputVal] = useState("");
  const [selected, setSelected] = useState("");
  const [apiData, setApiData] = useState([]);
  const url = `https://api.edamam.com/api/recipes/v2?type=public&q=${inputVal}&app_id=bb59b48e&app_key=761ec311f73971b9db888cc25aee5c97%09&diet=${selected}&imageSize=REGULAR
  `;
  const submitValue = (e) => {
    e.preventDefault();
    debugger;
    axios.get(url).then((res) => {
      const persons = res.data;
      console.log(persons);
      // const c = res.data["hits"].map((t)=>{
      //   return t.recipe

      // })
      console.log(persons);
      setApiData(persons.hits);
    });

    const values = {
      items: inputVal,
      select: selected,
    };
    console.log(values);
    console.log(url);
  };
  const newtab = (imgurl) => {
    alert("hi");
    debugger;
    window.open(imgurl);
  };

  return (
    <div className="" style={{ marginTop: "6%" }}>
      {/* <h1>Food Recepies</h1> */}
      <div className="row">
        <div
          id="carouselExampleIndicators"
          class="carousel slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="0"
              class="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2017/01/11/11/33/cake-1971552__480.jpg"
              />{" "}
            </div>
            <div class="carousel-item">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2018/12/10/23/32/cafe-3868124__480.jpg"
              />
            </div>
            <div class="carousel-item">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2017/11/08/22/18/spaghetti-2931846__340.jpg"
              />
            </div>
          </div>
          <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>

      <form>
        <div className="row mt-5">
          <div class=" col-md-2"></div>
          <div class=" col-md-3">
            <input
              type="text"
              class="form-control"
              id="exampleFormControlInput1"
              placeholder="Search here"
              value={inputVal}
              onChange={(e) => setInputVal(e.target.value)}
            />
          </div>
          <div class=" col-md-3">
            <select
              id="cars"
              class="form-control"
              onChange={(e) => {
                setSelected(e.target.value);
              }}
            >
              <option
                value="balanced"
                onChange={(e) => setSelected("balanced")}
              >
                balanced
              </option>
              <option
                value="high-fiber"
                onChange={(e) => setSelected("high-fiber")}
              >
                high-fiber
              </option>
              <option
                value="high-protein"
                onChange={(e) => setSelected("high-protein")}
              >
                high-protein
              </option>
              <option
                value="low-carb"
                onChange={(e) => setSelected("low-carb")}
                selected
              >
                low-carb
              </option>
              <option
                value="low-fat"
                onChange={(e) => setSelected("low-fat")}
                selected
              >
                low-fat
              </option>
              <option
                value="low-sodium"
                onChange={(e) => setSelected("low-sodium")}
                selected
              >
                low-sodium
              </option>
            </select>
          </div>
          <div className="col-md-1">
            <button
              type="button"
              class="btn btn-primary btn-md btn-block"
              onClick={submitValue}
            >
              {/* <button type="button" class="btn btn-primary btn-lg btn-block">Block </button> */}
              Submit
            </button>
          </div>
        </div>
      </form>
      <div className="row mt-5">
        {apiData.map((val) => {
          return (
            <>
              <div className="col-md-4">
                <div class="card">
                  <img
                    src={val.recipe.image}
                    onClick={() => newtab(val.recipe.url)}
                    class="card-img-top"
                    alt="..."
                  />
                  <div class="card-body">
                    {/* <h5 class="card-title"> {val.recipe.label}</h5>
                    <h5 class="card-title">Calories : {val.recipe.calories}</h5>
                    <h5 class="card-title">
                      Main ingredients : {val.recipe.ingredients[0].text}
                    </h5> */}
                    <h5 class="card-title">
                      {/* <b>{val.recipe.label}</b>{" "} */}
                      <label for="formGroupExampleInput"><b>Label</b> </label>

                      <input
                          type="text"
                          class="form-control"
                          id="formGroupExampleInput"
                          placeholder="Calories"
                          value={val.recipe.label}
                          readonly
                        />
                    </h5>
                    <form>
                    <div class="form-group">
                        {/* <label for="formGroupExampleInput">Calories : </label> */}
                        
                      </div>
                      <div class="form-group">
                        <label for="formGroupExampleInput"><b>Calories</b> </label>
                        <input
                          type="text"
                          class="form-control"
                          id="formGroupExampleInput"
                          placeholder="Calories"
                          value={val.recipe.calories}
                          readonly
                        />
                      </div>
                      <div class="form-group">
                        <label for="formGroupExampleInput2">
                          <b>Main ingredients</b> 
                        </label>
                        <input
                          type="text"
                          class="form-control"
                          id="formGroupExampleInput2"
                          placeholder="Main ingredients"
                          value={val.recipe.ingredients[0].text}
                          readonly
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </>
          );
        })}
      </div>
    </div>
  );
}
