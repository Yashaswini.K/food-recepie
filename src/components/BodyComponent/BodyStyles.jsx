import React from "react";
import { blueGrey } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme)=>({
    section:{
      margin:theme.spacing(3,0),
  
    },
    pageTitle:{
      color:blueGrey[800],
      marginBottom:theme.spacing(2),
      textTransform:"capitalize",
    },
    pageSubTitle:{
      color:blueGrey[500],
      marginBottom:theme.spacing(1,0),
      textTransform:"uppercase",
    },
    cardLabel:{
        textTransform:"uppercase",
        color:blueGrey[500],
        textAlign:"center",
        margin:theme.spacing(1,0),
        [theme.breakpoints.down("xs")]:{
            fontSize:"0.8rem"
        }
    },
    cardTitle:{
        textTransform:"capitalize",
        color:blueGrey[800],
        textAlign:"center",
        margin:theme.spacing(1,0),
        [theme.breakpoints.down("xs")]:{
            fontSize:"1.8rem"
        }
    },
    ratioBtn:{
        fontSize:"1rem",
        fontWeight:"bold",
        
    }
  }))
