import React, { useState } from "react";

export default function Link() {
  const [details, setDetails] = useState({});
  const [foodData, setFoodData] = useState([]);
  const [isstate, setIsState] = useState(false);
  const [eid, setEid] = useState("");
  const save = () => {
    debugger;
    if (eid === "") {
      console.log(details);
      let ing = details.ingridence.split(",");
      console.log(ing);
      debugger;
      let newdetails = {
        receipe: details.receipe,
        ingridence: ing,
      };

      setFoodData([...foodData, newdetails]);
      setDetails({});
      setDetails({
        receipe: "",
        ingridence: "",
      });
    } else {
      debugger;
      let ing = details.ingridence.split(",");
      console.log(ing);
      debugger;
      let newdetails = {
        receipe: details.receipe,
        ingridence: ing,
      };
      debugger;
      //  let data= foodData[eid]=newdetails;
      let copyfood = [...foodData];
      copyfood.splice(eid, 1, newdetails);
      setFoodData(copyfood);
      setEid("");
    }

    console.log(foodData);
    debugger;
    setDetails({});
    setDetails({
      receipe: "",
      ingridence: "",
    });
    console.log(details);
    // setIsState(false);
  };
  const editVal = (id) => {
    setEid(id);
    debugger;
    setIsState(true);
    let ingjoin = foodData[id].ingridence.join();
    console.log(ingjoin);
    setDetails({
      receipe: foodData[id].receipe,
      ingridence: ingjoin,
    });
  };
  const deleteVal = (id) => {
    debugger;
    let copydata = [...foodData];
    copydata.splice(id, 1);
    setFoodData(copydata);
    setEid("");
  };
  const Addbtn = () => {
    setIsState(true);
  };
  return (
    <div style={{ marginTop: "6%" }}>
      <div className="row">
        <div
          id="carouselExampleIndicators"
          class="carousel slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="0"
              class="active"
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleIndicators"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2015/05/07/15/08/cookie-756601_1280.jpg"
              />{" "}
            </div>
            <div class="carousel-item">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2016/03/23/15/00/ice-cream-1274894__340.jpg"
              />
            </div>
            <div class="carousel-item">
              <img
                width="100%"
                height="300px"
                src="https://cdn.pixabay.com/photo/2017/05/07/08/56/pancakes-2291908__480.jpg"
              />
            </div>
          </div>
          <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>

      <div className="row">
        <div class="col-md-5 mt-4 "></div>
        <div class="col-md-2 mt-4 ">
          <button
            type="button"
            class="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
            onClick={Addbtn}
            // style={{ position: "fixed" }}
          >
            Add Receipe
          </button>
        </div>
        <div class="col-md-4 mt-4 "></div>
      </div>
      <div className="row">
        <div class="col-md-4 mt-3 "></div>
        <div class="col-md-4">
          <div class="mb-3 mt-5"></div>
          {foodData.map((val, index) => {
            return (
              <>
                <div
                  className="col-md-4"
                  class="card"
                  style={{ textAlign: "center", justifyContent: "center" }}
                >
                  <div data-bs-toggle="collapse" data-bs-target={`#a${index}`}>
                    <div class="card-header">
                      <b>{val.receipe}</b>
                    </div>
                    {/* <h5 class="card-header"></h5>  */}
                    <div class="card-body"></div>

                    <div id={`a${index}`} class="collapse">
                      <h6>Ingredient</h6>
                      {val.ingridence.map((ele, ind) => {
                        return (
                          <>
                            <p class="card-text">{ele}</p>
                          </>
                        );
                      })}

                      <button
                        style={{ marginBottom: "5%" }}
                        class="btn btn-primary"
                        data-bs-toggle="modal"
                        data-bs-target="#exampleModal"
                        onClick={() => {
                          editVal(index);
                        }}
                      >
                        Edit
                      </button>
                      <button
                        style={{ marginLeft: "6%", marginBottom: "5%" }}
                        class="btn btn-danger"
                        onClick={() => {
                          deleteVal(index);
                        }}
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                </div>
              </>
            );
          })}
        </div>
        <div class="col-md-4"></div>
      </div>
      {isstate && (
        <div
          class="modal fade"
          id="exampleModal"
          tabindex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                   Recipe List
                </h5>
                <button
                  type="button"
                  class="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div class="modal-body">
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">
                    Reciepe
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="name"
                    value={details.receipe}
                    onChange={(e) => {
                      setDetails({ ...details, receipe: e.target.value });
                    }}
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">
                    Ingridence
                  </label>
                  <input
                    type="text"
                    class="form-control"
                    id="ing"
                    value={details.ingridence}
                    onChange={(e) => {
                      setDetails({ ...details, ingridence: e.target.value });
                    }}
                  />
                </div>
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" class="btn btn-primary" onClick={save}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
