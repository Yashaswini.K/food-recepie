import {
  Box,
  Grid,
  Card,
  Typography,
  CardContent,
  Button,
} from "@material-ui/core";
// import { PageHeader } from "../../Common/CommonComponent";
import { useStyles } from "../BodyStyles";
// import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
// import { DisplaycardGraph } from "../../Common/GraphComponent";
import { useEffect } from "react";
import { PieChart } from "react-minimal-pie-chart";
import "./DashboardStyle.css";
export default function Dashboard() {
  const classes = useStyles();

  const btn = document.querySelector(".read-more-btn");
  const text = document.querySelector(".card_read-more");
  const cardholder = document.querySelector(".card-holder");
  const readmore = (e) => {
    const current = e.target;
    const isReadMoreBtn = current.className.includes("read-more-btn");
    if (!isReadMoreBtn) return;
    const currentText = e.target.parentNode.querySelector(".card_read-more");
    currentText.classList.toggle("card_read-more--open");
    current.textContent = current.textContent.includes("Read More..")
      ? "Read Less..."
      : "Read More...";
  };
  // cardholder.addEventListener('click',e=>{
  //   const current = e.target;
  //   const isReadMoreBtn = current.className.includes('read-more-btn')
  //   if(!isReadMoreBtn)
  //   return;
  //   const  currentText = e.target.parentNode.querySelector('.card_read-more')
  //   currentText.classList.toggle('card_read-more--open');
  //   current.textContent = current.textContent.includes('Read More..')? 'Read Less...':'Read More...';
  // })

  return (
    <Box>
      {/* <PageHeader label="Dashboard" pageTitle="blog Overview" /> */}
      <Grid container>
        <div className="card-holder mt-5" onClick={readmore}>
          <div class="card">
            <img
              src="https://cdn.pixabay.com/photo/2018/04/13/17/14/vegetable-skewer-3317060_1280.jpg"
              class="card_img"
              alt="..."
            />
            <div class="card-contents">
              <h5 class="card_name" style={{ textAlign: "justify" }}>
                Vegetable Stick
              </h5>
              <h6 style={{ textAlign: "justify" }}>
                pieces of raw vegetables (such as celery or carrot sticks)
                served as an hors d'oeuvre often with a dip.
              </h6>
              <span className="card_read-more" style={{ textAlign: "justify" }}>
                There’s a scientific consensus that a balanced, rotating diet of
                different varieties of vegetables is one of the best ways to
                source nutrients from your food starting at a young age.
              </span>
              <br></br>
              <p className="read-more-btn">Read More...</p>
            </div>
          </div>
          <div class="card">
            <img
              src="https://cdn.pixabay.com/photo/2014/11/05/15/57/salmon-518032__340.jpg"
              class="card_img"
              alt="..."
            />
            <div class="card-contents">
              <h5 class="card_name">Meat</h5>
              <h6 style={{ textAlign: "justify" }}>
                Meat, the flesh or other edible parts of animals (usually
                domesticated cattle, swine, and sheep) used for food.
              </h6>
              <span className="card_read-more" style={{ textAlign: "justify" }}>
                Including not only the muscles and fat but also the tendons and
                ligaments. Meat is valued as a complete protein food containing
                all the amino acids necessary for the human body.
              </span>
              <br></br>
              <p className="read-more-btn">Read More...</p>
            </div>
          </div>
          <div class="card">
            <img
              src="https://cdn.pixabay.com/photo/2014/05/23/23/17/dessert-352475_1280.jpg"
              class="card_img"
              alt="..."
            />
            <div class="card-contents">
              <h5 class="card_name">Coup Cake</h5>
              <h6 style={{ textAlign: "justify" }}>
                cupcake (also British English: fairy cake; Hiberno-English: bun)
                is a small cake designed to serve one person.
              </h6>
              <span className="card_read-more" style={{ textAlign: "justify" }}>
                Which may be baked in a small thin paper or aluminum cup. As
                with larger cakes, frosting and other cake decorations such as
                fruit and candy may be applied.
              </span>
              <br></br>
              <p className="read-more-btn">Read More...</p>
            </div>
          </div>
          <div class="card">
            <img
              src="https://cdn.pixabay.com/photo/2014/11/30/10/48/chocolate-551424__480.jpg"
              class="card_img"
              alt="..."
            />
            <div class="card-contents">
              <h5 class="card_name">Chocolate</h5>
              <h6 style={{ textAlign: "justify" }}>
                Chocolate is a food product made from roasted and ground cacao
                pods, that is available as a liquid, solid or paste.
              </h6>
              <span className="card_read-more" style={{ textAlign: "justify" }}>
               Much of the chocolate consumed today is in the form of sweet chocolate,
                a combination of cocoa solids, cocoa butter or added vegetable
                oils, and sugar.
              </span>
              <br></br>
              <p className="read-more-btn">Read More...</p>
            </div>
          </div>
        </div>
      </Grid>
    </Box>
  );
}
