import React from "react";
import { Button, List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/Dashboard";
import BookIcon from "@material-ui/icons/Book";
import PostAddIcon from "@material-ui/icons/PostAdd";
import NotificationsIcon from "@material-ui/icons/Notifications";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { NavLink } from "react-router-dom";
import { useStyles } from "./HeaderStyles";

export default function SidenavData({handleDrawerClose}) {
  const classes = useStyles();

  const listItemData = [
    { label: "Dashboard", link: "/", icon: <DashboardIcon /> },
    { label: "Search Food", link: "/blog", icon: <BookIcon /> },
    { label: "Add Receipe", link: "/link", icon: <PostAddIcon /> },
    {
      label: "Add Food",
      link: "/notification",
      icon: <NotificationsIcon />,
    },
    { label: "Logout", link: "/", icon: <ExitToAppIcon /> },
  ];
  return (
    <List>
      {listItemData.map((item, i) => {
        
        console.log("side bar")
        return(
        <Button size="small" className={classes.navButton} onClick={()=>handleDrawerClose()}>
        <ListItem
          component={NavLink}
          to={item.link}
          className={classes.navlinks}
          activeClassName={classes.activeNavlinks}
          key={i}
        >
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText>{item.label}</ListItemText>
        </ListItem>
        </Button>
        )
})}
    </List>
  );
}
