import React, { useState } from "react";
import { Hidden, Drawer, Paper, Typography } from "@material-ui/core";
import { useStyles} from './HeaderStyles';
import {makeStyles } from '@material-ui/core';
import { blue } from '@material-ui/core/colors';
import './sidenavStyles.css'
import SidenavData from "./SidenavData";


export default function Sidenav({mobileOpen,handleDrawerOpen,handleDrawerClose}) {
  const classes = useStyles;

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden mdUp implementation="css" >
        <Drawer
          variant="temporary"
          anchor={"left"}
          open={mobileOpen}
          onClose={handleDrawerOpen}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <SidenavData handleDrawerClose={handleDrawerClose}/>
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
        
          variant="permanent"
          open
        >
            <SidenavData handleDrawerClose={handleDrawerClose}/>
        </Drawer>
      </Hidden>
    </nav>
  );
}
