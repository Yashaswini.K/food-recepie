import React from "react";
import {
  AppBar,
  Toolbar,
  Menu,
  Hidden,
  Typography,
  Button,
  makeStyles,
  Box,
  IconButton,
} from "@material-ui/core";
import Profile from "./Navtabs/Profile";
import Notification1 from "./Navtabs/Notification1";
import { useStyles } from "./HeaderStyles";
import Messages from "./Navtabs/Messages";
import MenuIcon from "@material-ui/icons/Menu";

export default function Navbar({handleDrawerOpen}) {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar className={classes.toolbar}>
        <Typography variant="h6" className={classes.logo}>
          {"Food Recipes"}
        </Typography>
        <Hidden smDown>
          <Box style={{ display: "flex" }}>
            <Notification1 />
            <Messages />
            <Profile />
          </Box>
        </Hidden>
        <Hidden mdUp>
        <IconButton color="inherit" onClick={handleDrawerOpen}>
          <MenuIcon />
        </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}
