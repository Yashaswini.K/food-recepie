import { Route, Routes } from 'react-router-dom';
import React,{useState} from 'react';
import Navbar from './Navbar';
import Sidenav from './Sidenav';
import Dashboard from '../BodyComponent/Dashboard/Dashboard';
import BlogComponent from '../BodyComponent/BlogComponent';
import Link from '../BodyComponent/Link';
import Notification from '../BodyComponent/Notification';
import Logout from '../BodyComponent/Logout';
import { Box } from '@material-ui/core';
import { CallMissedSharp } from '@material-ui/icons';
import { useStyles } from './HeaderStyles';
import Register from '../Register';
import Login from '../Login';

function HeaderComponent() {
  const classes = useStyles();
  const [mobileOpen,setMobileOpen]=useState(false);
  const handleDrawerOpen = () => {
    setMobileOpen(!mobileOpen)
  }
  const handleDrawerClose = () => {
    setMobileOpen(false)
  }

  return (
      <div>
         <Navbar mobileOpen ={mobileOpen} handleDrawerOpen={handleDrawerOpen}/>
         <Sidenav mobileOpen ={mobileOpen} handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose}/>
         {/* registration routes */}
         <Box className={classes.wrapper}>
         <Routes>
         {/* <Route exact path="/" element={<Register/>}/>
         <Route exact path="/login" element={<Login/>}/> */}
           <Route exact path="/" element={<Dashboard/>}/>
           <Route exact path="/blog" element={<BlogComponent/>}/>
           <Route exact path="/link" element={<Link/>}/>
           <Route exact path="/notification" element={<Notification/>}/>
           <Route exact path="/" element={<Logout/>}/>
         </Routes>
         </Box>
        
      </div>
  )
}

export default HeaderComponent;
