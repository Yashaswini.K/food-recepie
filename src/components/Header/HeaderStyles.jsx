import {makeStyles } from '@material-ui/core';
import { blue , blueGrey} from '@material-ui/core/colors';

export const useStyles = makeStyles((theme)=>({
    toolbar:{
        display:'flex',
        flexFlow:'row wrap',
        justifyContent:"space-between",
        position: "fixed",
        top: "0",
        width: "100%",
        backgroundColor:"blue"
    },
    logo:{
        color:"white",
    },
    navlist:{
        minWidth:"250px",
        maxWidth:"300px"
    },
    navAvatar:{
        width:"35px",
        height:"35px",
    },
    ulAvatar:{
        backgroundColor:blue["A200"],
        color:"white"
    },
    //side nav
    drawerPaper:{
        width:" 250px !important",
        marginTop:" 65px !important",
        [theme.breakpoints.down("sm")]:{
            marginTop:"0px",
        },
    },
    //wrapper of the main container
    wrapper:{
        height:"100vh",
        // background:"#efefef",
        padding:theme.spacing(2,2,0,34),
        [theme.breakpoints.down("xs")]:{
            padding:theme.spacing(2,2),
        },
    },
    navlinks:{
        color:blueGrey["A400"],
        "& :hover , &:hover div":{
            color:blue["A400"],
        },
       " & div":{
            color:blueGrey["A400"],
        },
    },
    activeNavlinks:{
        color:blue["A700"],
        " & div":{
            color:blue["A700"],
        },
    },
    navButton:{
        width:"100%",
        textTransform:"capitalize"
    }
}))