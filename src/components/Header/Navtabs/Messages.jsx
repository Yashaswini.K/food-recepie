import React, { useState } from "react";
import { Menu, MenuItem, Button, Box, IconButton,Badge,ListItemText, ListItem, ListItemIcon, Avatar, List } from "@material-ui/core";
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import image from './image.webp';
import { useStyles } from "../HeaderStyles";

export default function Messages() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const dropDownData = [
      {label:"Alex",desc:'likes you fedd ...'},
      {label:"Frexa",desc:'likes you fedd ...'},
      {label:"Dazer",desc:'likes you fedd ...'},
      {label:"Lobie",desc:'likes you fedd ...'},
  ]
  return (
    <Box>
      <IconButton
        aria-controls="Messages"
        aria-haspopup="true"
        onClick={handleClick}
        color="inherit"
      >
        <Badge badgeContent={4} color="secondary">
            <ChatBubbleIcon />
        </Badge>
      </IconButton>
      <Menu
        id="Messages"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}>
          <List className={classes.navlist}>
      {dropDownData.map((item,i)=>
      <ListItem key={i}  onClick={handleClose}>
          <ListItemIcon>
              <Avatar className={classes.ulAvatar}>{item.label[0].toUpperCase()}</Avatar>
          </ListItemIcon>
          <ListItemText primary={item.label} secondary={item.desc}>
          </ListItemText>     
           </ListItem>
      )}
        </List>
      </Menu>
    </Box>
  );
}
