import React, { useState } from "react";
import { Menu, MenuItem, Button, Box, IconButton,Badge, ListItem, ListItemIcon, Avatar } from "@material-ui/core";
import NotificationsIcon from '@material-ui/icons/Notifications';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import image from './image.webp'
import { mergeClasses } from "@material-ui/styles";
import { useStyles } from "../HeaderStyles";

export default function Profile() {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const dropDownData = [
      {label:"settings",icon:<SettingsIcon/>},
      {label:"Logout",icon:<ExitToAppIcon/>},
  ]
  return (
    <Box>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
        startIcon={<Avatar src={image} className={classes.navAvatar}></Avatar>}
      >
       
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}>
      {dropDownData.map((item,i)=>
      <MenuItem key={i} component={ListItem} onClick={handleClose}>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemIcon>{item.label}</ListItemIcon>
      </MenuItem>
      )}
        
      </Menu>
    </Box>
  );
}
